# MyListView

## 效果演示
![](https://images.gitee.com/uploads/images/2021/0916/153955_4a149f28_8627638.gif "listview.gif")

## 介绍
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;目前很多手机app展示最常用的就是listview，下拉刷新、上拉加载更多基于这个集成一个listview。另外此listview支持上下切换item项，
并提供了切换后的事件捕获，以及主动获取切换步骤的方法。为了性能考虑了item的复用机制，不过在previewer预览时，快速滑动的时候会偶发展示问题，后期排查改进。

## 属性

| 名称              | 类型                           | 默认值                | 必填 | 描述                                                         |
| ---------------- | ------------------------------ | --------------------- | ---- | ------------------------------------------------------------ |
| field             | [FieldConfig](#FieldConfig)    | {id: 'id',url: 'url',content: 'content'}| 否   | 传入的数据属性和组件内部使用的相关属性进行匹配设置           |
| draggable         | boolean                        | true                  | 否   | 列表是否开启拖拽模式                    |
| list              | Array<Object>                  | []                    | 否   | 传入的数组数据                                               |
| height            | number                         | 60                    | 否   | list列表中每一项item的高度                                   |
| imageWidth        | number                         | 50                    | 否   | 拖拽图标的高度                                               |
| loading           | boolean                        | false                 | 否   | 是否处于加载数据状态，加载过程中不触发load事件                   |
| finished          | boolean                        | false                 | 否   | 是否已加载完成，加载完成后不再触发load事件                   |
| error             | boolean                        | false                 | 否   | 是否加载失败，加载失败后点击错误提示可以重新触发load事件     |
| fresh             | boolean                        | false                 | 否   | 是否下拉刷新，加载数据中                                         |
| isItemResume      | boolean                        | false                 | 否   | 是否支持item复用                                    |

<div id="FieldConfig">表1 FieldConfig</div>

| 名称    | 类型   | 默认值  | 必填 | 描述                                               |
| ------- | ------ | ------- | ---- | -------------------------------------------------- |
| id      | string | id      | 否   | 传入的数据id属性和组件内部使用的id属性进行匹配设置 |
| url     | string | url     | 否   | 拖拽区域的icon对应的字段和传入的字段匹配设置       |
| content | string | content | 否   | item右侧展示的内容对应的字段和传入的字段匹配设置   |

## 事件

| 名称        | 参数                                              | 描述                                                         |
| ----------- | ------------------------------------------------- | ------------------------------------------------------------ |
| drag-over  | {processRecord:[],allProcessRecords: []} | 单次拖拽结束事件,参数当前拖拽步骤记录（数据的id）、历史每次拖拽步骤记录 |
| refresh-date | -                                                | 下拉刷新                                               |
| load        | -                                                | 滚动到底部触发，加载下一页数据                                         |

## 方法

| 名称        | 参数                                              | 描述                                                         |
| ----------- | ------------------------------------------------- | ------------------------------------------------------------ |
| getProcessRecord  |无   | 当前拖拽步骤记录（数据的id） |
| getAllProcessRecords | 无   | 历史每次拖拽步骤记录（数据的id）                                           |

## 示例
index.html
````html
    <mylistview id="mylist" field="{{ field }}" list="{{ list }}" is-item-resume="{{ false }}"
                loading="{{ loading }}" finished="{{ finished }}" error="{{ error }}" @refresh-date="pullDown" fresh="{{fresh}}"
                @load="loadData" @drag-over="onDragOver">
    </mylistview>
````
index.js
````javascript
import prompt from '@system.prompt';
const SPLICE_LENGTH=20; // 模拟数据每次截取的长度
var count = 0;
export default {
	data: {
		list: [],//页面展示数据
		field: {//数据源字段名
			id: 'ID',
			content: 'text',
			url: 'path'
		},
		loading: false,//是否数据加载中
		finished: false,//分页是否到底部了
		error: false,//数据加载出错
		page: 0,//页码
		fresh:false,//是否下拉刷新 需要与loading区分开
	},

	onReady() {
		this.initMyData();
	},
	/**
	* 模拟从数据或者文件获取数据，这里因为是模拟，所以页码为用上
  */
	getDate(pageIndex){
		this.loading = true;

		//模拟接口耗时获取数据
		let beginTime=new Date().getTime();
		while(new Date().getTime()  < beginTime + 500) {   }

		let d = ["caidie", "caihuadie", "huangfeng", "hudie", "hujiachong", "jiachong", "lvdouying", "mafeng",
		"maomaochong", "mayi", "mazhazi", "mifeng", "pangxiao", "pulee", "qingting", "qiuyin", "tiaozao", "wenzi",
		"yamaidie", "zhizhu", "caidie", "caihuadie", "huangfeng", "hudie", "hujiachong", "jiachong", "lvdouying", "mafeng",
		"maomaochong", "mayi", "mazhazi", "mifeng", "pangxiao", "pulee", "qingting", "qiuyin", "tiaozao", "wenzi",
		"yamaidie", "zhizhu"];
		let listStore = [];

		for (let i = 0;i < SPLICE_LENGTH; i++) {
			listStore.push({
				ID: i+SPLICE_LENGTH*(this.page-1) ,
				text: `${d[i]}——${i+SPLICE_LENGTH*(this.page-1) }`,
				path: `common/images/icon/${d[i%d.length]}.png`
			});
		}
		this.loading = false;
		if(this.fresh) this.fresh = false ;
		return listStore;
	},
	initMyData() {
		this.page=1;
		this.list = this.getDate(this.page);
		this.finished = false;
		this.error = false;
	},
	/**
	 * 下拉刷新
	  */
	pullDown(e) {
		this.fresh = true;
		this.initMyData();
	},
	/**
	* 加载更多
  	*/
	loadData(e) {
		this.page++;
		count++;
		if (count === 1) { // 模拟 请求数据加载失败时的提示
			this.page--;
			this.loading = true;
			setTimeout(() => {
				this.loading = false;
				this.error = true; // 显示数据加载失败提示
			}, 1500)
		} else {
			this.error = false;
			let data = e.detail;
			let currentPageList = this.getDate(this.page);
			if (currentPageList.length > 0 && this.page < 4) {//模拟数据加载下一页
				this.loading = true;
				setTimeout(() => {
					this.list = [...e.detail, ...currentPageList];
					this.loading = false;
				}, 1500)
			} else {//模拟数据已经最后一页，无下一页数据情况
				this.loading = true;
				setTimeout(() => {
					this.loading = false;
					this.finished = true;
				}, 1500)
			}
		}
	},
	onDragOver(e){
		//从组件属性当中获取拖拽路径，用于逻辑处理
		let processRecord = JSON.stringify(this.$child('mylist').getProcessRecord());
		let allProcessRecords = JSON.stringify(this.$child('mylist').getAllProcessRecords());

		//从事件当中获取拖拽路径，用于逻辑处理
//		let processRecord = JSON.stringify(e.detail.processRecord);
//		let allProcessRecords = JSON.stringify(e.detail.allProcessRecords);
		console.log(""+processRecord);
		console.log(""+allProcessRecords)

		prompt.showToast({
			message: "最后一次拖拽步骤:"+processRecord+";所有的拖拽步骤:"+allProcessRecords+"。"
		})
	}
}

````

## 代码参考
[https://gitee.com/chinasoft6_ohos/MyListView](https://gitee.com/chinasoft6_ohos/MyListView)

作者：陈巧银 瞿纬乾

