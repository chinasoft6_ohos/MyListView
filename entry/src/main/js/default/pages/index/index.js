import prompt from '@system.prompt';
const SPLICE_LENGTH=20; // 模拟数据每次截取的长度
var count = 0;
export default {
	data: {
		list: [],//页面展示数据
		field: {//数据源字段名
			id: 'ID',
			content: 'text',
			url: 'path'
		},
		loading: false,//是否数据加载中
		finished: false,//分页是否到底部了
		error: false,//数据加载出错
		page: 0,//页码
		fresh:false,//是否下拉刷新 需要与loading区分开
	},

	onReady() {
		this.initMyData();
	},
	/**
	* 模拟从数据或者文件获取数据，这里因为是模拟，所以页码为用上
  */
	getDate(pageIndex){
		this.loading = true;

		//模拟接口耗时获取数据
		let beginTime=new Date().getTime();
		while(new Date().getTime()  < beginTime + 500) {   }

		let d = ["caidie", "caihuadie", "huangfeng", "hudie", "hujiachong", "jiachong", "lvdouying", "mafeng",
		"maomaochong", "mayi", "mazhazi", "mifeng", "pangxiao", "pulee", "qingting", "qiuyin", "tiaozao", "wenzi",
		"yamaidie", "zhizhu", "caidie", "caihuadie", "huangfeng", "hudie", "hujiachong", "jiachong", "lvdouying", "mafeng",
		"maomaochong", "mayi", "mazhazi", "mifeng", "pangxiao", "pulee", "qingting", "qiuyin", "tiaozao", "wenzi",
		"yamaidie", "zhizhu"];
		let listStore = [];

		for (let i = 0;i < SPLICE_LENGTH; i++) {
			listStore.push({
				ID: i+SPLICE_LENGTH*(this.page-1) ,
				text: `${d[i]}——${i+SPLICE_LENGTH*(this.page-1) }`,
				path: `common/images/icon/${d[i%d.length]}.png`
			});
		}
		this.loading = false;
		if(this.fresh) this.fresh = false ;
		return listStore;
	},
	initMyData() {
		this.page=1;
		this.list = this.getDate(this.page);
		this.finished = false;
		this.error = false;
	},
	/**
	 * 下拉刷新
	  */
	pullDown(e) {
		this.fresh = true;
		this.initMyData();
	},
	/**
	* 加载更多
  	*/
	loadData(e) {
		this.page++;
		count++;
		if (count === 1) { // 模拟 请求数据加载失败时的提示
			this.page--;
			this.loading = true;
			setTimeout(() => {
				this.loading = false;
				this.error = true; // 显示数据加载失败提示
			}, 1500)
		} else {
			this.error = false;
			let data = e.detail;
			let currentPageList = this.getDate(this.page);
			if (currentPageList.length > 0 && this.page < 4) {//模拟数据加载下一页
				this.loading = true;
				setTimeout(() => {
					this.list = [...e.detail, ...currentPageList];
					this.loading = false;
				}, 1500)
			} else {//模拟数据已经最后一页，无下一页数据情况
				this.loading = true;
				setTimeout(() => {
					this.loading = false;
					this.finished = true;
				}, 1500)
			}
		}
	},
	onDragOver(e){
		//从组件属性当中获取拖拽路径，用于逻辑处理
		let processRecord = JSON.stringify(this.$child('mylist').getProcessRecord());
		let allProcessRecords = JSON.stringify(this.$child('mylist').getAllProcessRecords());

		//从事件当中获取拖拽路径，用于逻辑处理
//		let processRecord = JSON.stringify(e.detail.processRecord);
//		let allProcessRecords = JSON.stringify(e.detail.allProcessRecords);
		console.log(""+processRecord);
		console.log(""+allProcessRecords)

		prompt.showToast({
			message: "最后一次拖拽步骤:"+processRecord+";所有的拖拽步骤:"+allProcessRecords+"。"
		})
	}
}
