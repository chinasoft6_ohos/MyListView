export default {
	data() {
		return {
			globalY: 0, // 拖拽的前的垂直方向位置
			totalHeight: 0,
			idx: 0,
			idx_data: {},
			lastScrollElementId: '',
			dataList: [], //界面显示的数据
			list_state: [], //实际列表拥有的数据，从属性list拿来，切换item项时需要被变换
			reUseCount: 0, //界面可被复用的item的数目
			activelist: [], //界面展示列表的实际顺序，与界面绘制item项的自上而下顺序对应
			lastlenght: 0, //记录界面总共展示的item项数目
			lasty: 0, //记录滚动时，上一次y轴的位置（item复用使用）
			isFresh: false, //记录是否时刷新数据
			processRecord: [], // 当前拖拽操作的步骤记录
			allProcessRecords: [], // 所有拖拽操作的步骤记录
		}
	},
	props: {
		field: { // list元素对象属性需要设置的字段名称
			default: {
				id: 'id', // id字段
				url: 'url', // url字段
				content: 'content' // 内容
			}
		},
		draggable:{
			default:true // 设置false，便不可拖拽
		},
		list: { // 数据
			default: []
		},
		height: {
			default: 60 // item高度
		},
		imageWidth: {
			default: 50 // 图标宽度
		},
		loading: {
			default: false // 是否处于加载状态，加载过程中不触发load事件
		},
		finished: {
			default: false // 是否已加载完成，加载完成后不再触发load事件
		},
		error: {
			default: false // 是否加载失败，加载失败后点击错误提示可以重新触发load事件
		},
		fresh:{
			default: false // 判断是否刷新完成
		},
		isItemResume:{//设置是否支持item复用
			default: false
		}
	},
	onInit() {
		this.$watch('list', 'onPropertyChange');
	},
	/**
	 * list数据变化时数据处理
  	*/
	onPropertyChange() {
		this.list_state = this.list;
		if(!this.isItemResume || this.isItemResume==false) this.reUseCount = this.list.length;
		if (this.isFresh || this.dataList.length < this.reUseCount) {
			let data = []
			for (let i = 0; i < this.list.length && i < this.reUseCount; i++) {
				data.push(this.getData(this.list_state[i], i));
				this.activelist.push(i);
			}
			this.dataList = data;
			this.lastlenght = this.dataList.length;
		} else {
			this.reUseItem();
		}
		this.totalHeight = this.height * this.list_state.length;
		this.isFresh = false;
	},
	/**
	 *  将原始传入的数据转化成内部需要的数据
  	 */
	getData(res, i) {
		function _isUndefined(obj) {
			return typeof obj === "undefined";
		};
		return {
			id:i%this.reUseCount,
			data_id: _isUndefined(res[this.field.id]) ? i : res[this.field.id],
			url: _isUndefined(res[this.field.url]) ? '' : res[this.field.url],
			content: _isUndefined(res[this.field.content]) ? '' : res[this.field.content],
			top: i * this.height
		}
	},
	/**
	 *  计算出页面需要绘制的item数，其他的item复用
  	 */
	onLayoutReady() {
		var rect = this.$element('mylist').getBoundingClientRect();
		this.reUseCount = parseInt(rect.height / this.height) + 2;
		if(this.dataList.length==0){
			this.onPropertyChange();
		}
	},
	/**
	 * @description 获取被拖动的item项位置及数据
  	 */
	touchstartP(e) {
		if(!this.draggable){return};
		this.idx = parseInt(e.touches[0].localY / this.height); // 获取当前idx序号
		this.idx_data = this.list_state[this.idx]; // 获取第idx个item的数据
		this.processRecord = [];
	},
	/**
	 * 捕获冒泡事件，不再往上传递
  	*/
	grabTouchStart: function () {

	},
	/**
	 * 拖动item开始触摸
  	*/
	touchstart(id, e) {
		if(!this.draggable){return};
		this.globalY = e.touches[0].globalY; // 获取触摸开始位作为拖拽动作的起始位置
	},
	/**
	 * 拖动item开始移动
  	*/
	touchmove(id, e) {
		if(!this.draggable){return};
		let top = this.dataList[id].top + (e.touches[0].globalY - this.globalY); // 当前元素距离顶部的距离+移动的距离(+-)
		this.dataList[id].top = top; // 更新当前元素的距离
		this.changeItem(id, top);
		this.globalY = e.touches[0].globalY;
	},
	/**
	 * 拖动item结束拖动与触摸
  	*/
	touchend(id) {
		if(!this.draggable){return};
		this.dataList[id].top = this.idx * this.height;
		this.updateList(this.idx, this.idx_data);
		if(this.processRecord && this.processRecord.length>0) {
			this.allProcessRecords.push(this.processRecord);
			this.$emit("dragOver", {
				processRecord: this.processRecord,
				allProcessRecords: this.allProcessRecords
			}); // 拖拽结束事件,参数将拖拽步骤记录、及历史每次操作步骤记录
		}
	},
	/**
	 * @description 拖动item移动过程中，交换item项,idx永远记录的是空白的位置
	 * @param {Number} id item元素的在数组中的位置
	 * @param {Number} selectTop item元素的top属性值
     */
	changeItem(id, selectTop) {
		for (let i = 0;i < this.dataList.length; i++) {
			if (id == i) {
				continue;
			}
			let top = this.dataList[i].top;
			// 是否是向下拖拽&&拖拽一个位置
			if (parseInt(selectTop / this.height) > this.idx && parseInt((top + 10) / this.height) - 1 == this.idx) {
				this.dataList[i].top = this.idx * this.height; // 交换top位置

				this.processRecord=[this.idx_data[this.field.id], this.list_state[this.idx + 1][this.field.id]];

				this.updateList(this.idx, this.list_state[parseInt((top + 10) / this.height)]); // idx位置data值替换成了当前i位置的值
				this.idx = parseInt((top + 10) / this.height); // 当前i赋值给index
				break;
				// 是否是向上拖拽&&拖拽一个位置
			} else if (parseInt((selectTop + this.height) / this.height) < this.idx && parseInt((top + 10) / this.height) + 1 == this.idx) {
				this.dataList[i].top = this.idx * this.height;
				this.processRecord=[this.list_state[this.idx - 1][this.field.id], this.idx_data[this.field.id]];
				this.updateList(this.idx, this.list_state[parseInt((top + 10) / this.height)]);
				this.idx = parseInt((top + 10) / this.height);
				break;
			}
		}
	},
	/**
	 * 改变list数据位置，防止滚动后数据恢复原样
  	*/
	updateList(i, data) {
		this.list_state[i] = data;
	},
	refreshDate(e) {
		this.isFresh = true;
		this.$emit('refreshDate', e);
	},
	/**
	 *@description 滑动到底部
     */
	reachBottom() {
		// loading或 finished状态不触发load事件
		if (!(this.loading || this.finished)) {
			this.$emit("load", this.list_state);
		}
	},
	scroll(e) {
		if(this.isItemResume && this.isItemResume==true) this.reUseItem();
	},
	/***
	 * item项目复用
  	*/
	reUseItem() {
		if (this.isFresh) return;
		let xy = this.$element("mylist").currentOffset();
		if (xy.y > this.lasty && this.lastlenght < this.list_state.length && this.lastlenght < this.dataList.length + parseInt(xy.y / this.height)) { //上滑
			let item = this.activelist.splice(0, 1)[0];
			this.activelist.push(item);
			var list = [];
			for (var index = 0; index < this.dataList.length; index++) {
				if (index == item) {
					list.push(this.getData(this.list_state[this.lastlenght], this.lastlenght));
				} else {
					list.push(this.dataList[index])
				}

			}
			this.dataList = list;
			this.lastlenght += 1;
			this.lasty = xy.y;
		} else if (xy.y < this.lasty && this.lastlenght > this.dataList.length && this.lastlenght > this.dataList.length + parseInt((xy.y - this.height) / this.height)) { //下滑
			let item = this.activelist.pop();
			this.activelist.unshift(item);
			var list = [];
			for (var index = 0; index < this.dataList.length; index++) {
				if (index == item) {
					list.push(this.getData(this.list_state[this.lastlenght - this.dataList.length - 1], this.lastlenght - this.dataList.length - 1));
				} else {
					list.push(this.dataList[index])
				}

			}
			this.dataList = list;
			this.lastlenght -= 1;
			this.lasty = xy.y;
		}

	},
	getProcessRecord(){// 当前拖拽操作的步骤记录
		return this.allProcessRecords && this.allProcessRecords.length>0 ? this.allProcessRecords[this.allProcessRecords.length-1]:[];
	},
	getAllProcessRecords(){// 所有拖拽操作的步骤记录
		return this.allProcessRecords;
	}
}
